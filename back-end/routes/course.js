const express = require('express')
const router = express.Router()
const auth = require('../auth')
const CourseController = require('../controllers/course')

//get all courses
router.get('/getAll', (req, res) => {
    CourseController.getAll().then(courses => res.send(courses))
})

//get all ACTIVE courses
router.get('/getAllActive', (req, res) => {
    CourseController.getAllActive().then(courses => res.send(courses))
})

//get the course of the ID
router.get('/:courseId', (req, res) => {
	const courseId = req.params.courseId
    CourseController.get({ courseId }).then(course => res.send(course)) 
})

//Add courses
router.post('/', auth.verify, (req, res) => {
    CourseController.add(req.body).then(result => res.send(result))
})

//Update courses
router.put('/', auth.verify, (req, res) => {
	
    CourseController.update(req.body).then(result => res.send(result))
})

//Delete courses
router.delete('/:courseId', auth.verify, (req, res) => {
	const courseId = req.params.courseId
    CourseController.archive({ courseId }).then(result => res.send(result))
})

module.exports = router