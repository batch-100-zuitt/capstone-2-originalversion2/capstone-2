		let adminUser = localStorage.getItem("isAdmin");
		let userId = localStorage.getItem ("id");

        
		console.log(userId)


		
        
	
		// card footer will be dynamically rendered if the user is an admin or not
		let cardFooter;
		

		const url = (adminUser == "false" || !adminUser) ? 
		'http://localhost:4000/api/courses/getAllActive' : 
		'http://localhost:4000/api/courses/getAll'
        



		     fetch(url)
			.then(res => res.json())
			.then(data => {

		//log the data to check if you were able to fetch the data from the server
		console.log(data)
       
		//courseData will store the data to be rendered
	   let courseData
	  	   

		if(data.length < 1) {
			courseData = "No courses available"
				} else {
		//else iterate the courses collection and display each courses
			courseData = data.map(course => {
			

		//check the make up of each element inside the course collection
		//console.log(course._id)


		//if the user is regular user, dispaly when the course was created
		if(adminUser == "false" || !adminUser) {
              
			return (
			cardFooter=
			`                    
			  <div class="col-md-6 my-3">
                <div class= "card2">
                   <div class= "card-body2">

                        <h5 class= "card-title3">${course.name}</h5>
                        <p class= "card-text2 text-left">
                        ${course.description}             
                        </p>
                        <p class="card-text2 ">
                        <h5 class ="card-text2">Price: ${course.price}</h5>
                        </p> 

                        <div class = "nonAdminButton col-md-12">
                           <a href="./course.html?courseId=${course._id}" value= "course._id" 
                           class="btn btn-primary text-white btn-block col-md-8 editButton">
				           Select Course
				           </a>
				        </div> 

                     </div>         
                  </div>
               </div>				
			`
			)
			} else{
			//for admin user
              
            
             return (
            
             
			cardFooter =
			
			  `
		<div class="col-md-6 my-3">
            <div class= "cardAdmin2">                                            
              <div class= "card-bodyAdmin2"> 

                   	<h5 class= "card-titleAdmin2">${course.name}</h5>
                   		<p class= "card-text2 text-left">
                    		${course.description}             
                    	 </p>               		
                   			 <h5 class ="card-text2">Price: 
                   			 ${course.price}
                   			 </h5>                        
                   			<p class="card-isActive">Active:                   			  
                   			  <strong id="card-isActive">
                   			  ${course.isActive}
                   			  </strong> 
                   			</p>                 			                    			
                    		  <h5 class ="card-textAdmin2">Enrollees:
                    		    ${(course.enrollees).length}
                    		  </h5>              		         
			   		<div class="col-md-12 my-3">
			   		<div class="card2">

					<a href ="./editCourse.html?courseId=${course._id}" 
					value="{course._id}" class="btn btn-primary text-white btn-block editButton" >Edit</a>

					<a href="./deleteCourse.html?courseId=${course._id} " 
					value="{course._id}" class="btn btn-primary text-white btn-block dangerButton">Deactivate</a>

					</div>
					</div>

			</div>           
		  </div>		  
        </div>
				
			  `

			 	 	)	
          

                

			}
			       
       }).join(""); // dahil gumamit ng array

			

    }

let container = document.querySelector("#coursesContainer")

// get the value of courseData and assign it as the #courseContainer's content
container.innerHTML = courseData;
})


//add modal if user is an admin, there will be a button a course 
let modalButton = document.querySelector("#zuitterNav")
let indexCard = document.querySelector("#containerIndex")

if(adminUser == "false" || !adminUser) {
	//if user is regular user, do not show add course button
	modalButton.innerHTML = 
	`
	<div class="collapse navbar-collapse">

			<ul class="navbar-nav ml-auto" >
			         
				<li class="nav-item active">
					<a href="./../indexUser.html" class="nav-link"> Home </a>
				</li>
				
				<li class="nav-item ">
					<a href="./logout.html" class="nav-link"> Log out </a>
				</li>

				

			</ul>

		</div>

	`
}else{
	
modalButton.innerHTML = 
  ` 	  			   
			<div class="collapse navbar-collapse" id="zuitterNav">

			<ul class="navbar-nav ml-auto" >
			         

			 <li class="nav-itemAdmin">
	           <a href="./addCourse.html" class="nav-linkAdmin"> Add Course </a>
	          </li>
			
			<li class="nav-itemAdmin active">
				<a href="./../indexAdmin.html" class="nav-linkAdmin"> Home </a>
			</li>
				
			<li class="nav-itemAdmin">
				<a href="./logout.html" class="nav-linkAdmin"> Log out </a>
			</li>			
			</ul>
		</div>		


  `
  
 
}